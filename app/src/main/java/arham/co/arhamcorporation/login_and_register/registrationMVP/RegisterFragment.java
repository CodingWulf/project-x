package arham.co.arhamcorporation.login_and_register.registrationMVP;


import android.app.Activity;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthProvider;

import arham.co.arhamcorporation.R;
import arham.co.arhamcorporation.databinding.FragmentRegisterBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment implements View.OnClickListener,RegistrationView {

    private FragmentRegisterBinding mBinding;
    private View mView;
    private Context mContext;
    private Activity mActivity;
    private FirebaseAuth firebaseAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private static String TAG = RegisterFragment.class.getName();
    private RegistrationPresenterImpl mPresenter;
    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_register,container,false);
        mView = mBinding.getRoot();
        mContext = getActivity();
        mActivity = getActivity();
        renderRegistrationUI();
        return mView;
    }

    private void renderRegistrationUI()
    {
        firebaseAuth = FirebaseAuth.getInstance();
        mBinding.containerRegisterLayout.btnSignUp.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == mBinding.containerRegisterLayout.btnSignUp.getId())
        {
            if (mBinding.containerRegisterLayout.registerMobile.getText().toString().trim().equalsIgnoreCase(""))
            {
                mBinding.containerRegisterLayout.inputRegisterLayoutMobile.setError("Mobile can not be empty");
            }
            else {
                String phoneNumber = "+91"+mBinding.containerRegisterLayout.registerMobile.getText().toString();
                /*PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber
                ,60,TimeUnit.SECONDS,
                        mActivity,mCallbacks);*/
                mPresenter = new RegistrationPresenterImpl(this,new RegistrationInteractorImpl(mContext,mActivity));
                mPresenter.optConfirmationView();
                mPresenter.getMobileOTPSentConfirmation(phoneNumber);



            }
        }
    }

    @Override
    public void mobileOtpResponseView(boolean value) {
        Toast.makeText(mContext,"OTP Sent Successfully",Toast.LENGTH_SHORT).show();

    }
}
