package arham.co.arhamcorporation.login_and_register;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import arham.co.arhamcorporation.R;
import arham.co.arhamcorporation.common.AnimationFunctions;
import arham.co.arhamcorporation.common.CommonFuntions;
import arham.co.arhamcorporation.databinding.ActivityLoginUiBinding;
import arham.co.arhamcorporation.login_and_register.registrationMVP.RegisterFragment;


public class LoginActivityUI extends AppCompatActivity implements View.OnClickListener {
    private ActivityLoginUiBinding mBinding;
    private Context mContext;
    private LoginActivityUI mActivity;
    private float displayWidth;
    private float margin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mContext = this;
        mActivity = this;
        mBinding = DataBindingUtil.setContentView(mActivity, R.layout.activity_login_ui);
        displayWidth = CommonFuntions.getScreenWidth(mActivity);
        renderView();
    }


    private void renderView() {
        StartAnimations();
        loadFragment(new LoginFragment());
        mBinding.viewToAnimate.getLayoutParams().width = (int) displayWidth - (int) margin;
        mBinding.btnLogin.setOnClickListener(this);
        mBinding.btnRegister
                .setOnClickListener(this);
        //We will caculate the margin for getting the  proper display size for that particluar view
        ViewGroup.MarginLayoutParams vlp = (ViewGroup.MarginLayoutParams) mBinding.loginCareOptions.getLayoutParams();
        margin = vlp.leftMargin + vlp.rightMargin;

    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        anim.reset();
        mBinding.loginActivityContainer.clearAnimation();
        mBinding.loginActivityContainer.startAnimation(anim);
        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();

    }


    @Override
    public void onClick(View v) {

        //We subtracts the margin with total display for a particular view
        if (v.getId() == mBinding.btnRegister.getId()) {
            //Moving View
            AnimationFunctions.moveRight(mBinding.viewToAnimate, displayWidth - margin);
            mBinding.btnRegister.setTextColor(Color.WHITE);
            mBinding.btnLogin.setTextColor(Color.BLACK);
            loadFragment(new RegisterFragment());

        } else if (v.getId() == mBinding.btnLogin.getId()) {
            //Moving View
            AnimationFunctions.moveLeft(mBinding.viewToAnimate, displayWidth - margin);
            mBinding.btnRegister.setTextColor(Color.BLACK);
            mBinding.btnLogin.setTextColor(Color.WHITE);
            loadFragment(new LoginFragment());
        }


    }

    private void loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {

            if (fragment instanceof LoginFragment) {
                getSupportFragmentManager()
                        .beginTransaction().setCustomAnimations(R.anim.enter_from_left
                        , R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(mBinding.frameLoginOptionContainer.getId(), fragment)
                        .commit();
            } else {
                getSupportFragmentManager()
                        .beginTransaction().setCustomAnimations(R.anim.enter_from_right
                        , R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                        .replace(mBinding.frameLoginOptionContainer.getId(), fragment)
                        .commit();
            }

        }

    }
}






