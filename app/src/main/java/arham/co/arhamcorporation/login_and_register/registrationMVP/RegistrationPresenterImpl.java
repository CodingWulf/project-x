package arham.co.arhamcorporation.login_and_register.registrationMVP;

public class RegistrationPresenterImpl implements RegistrationPresenter, RegistrationInteractor.onMobileOTPSentResponse {

    private RegistrationView mView;
    private RegistrationInteractor mInteractor;

    public RegistrationPresenterImpl(RegistrationView mView, RegistrationInteractor mInteractor) {
        this.mView = mView;
        this.mInteractor = mInteractor;
    }

    @Override
    public void getMobileOTPSentConfirmation(String mobileNumber) {
        mInteractor.getMobileAuth(mobileNumber);

    }

    @Override
    public void optConfirmationView() {
        mInteractor.startOtpView(this);
    }

    @Override
    public void onSuccess() {

        mView.mobileOtpResponseView(true);
    }

    @Override
    public void onFailure() {
        mView.mobileOtpResponseView(false);
    }
}
