package arham.co.arhamcorporation.login_and_register;


import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import arham.co.arhamcorporation.R;
import arham.co.arhamcorporation.common.CommonFuntions;
import arham.co.arhamcorporation.databinding.FragmentLoginBinding;
import arham.co.arhamcorporation.homedesign.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener{

    private FragmentLoginBinding mBinding;
    private Context mContext;
    private View mView;
    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_login,container,false);
        mView = mBinding.getRoot();
        mContext = getActivity();

        renderFragmentUI();

        return mView;
    }

    private void renderFragmentUI()
    {
        mBinding.containerLoginLayout.loginPassword.setTransformationMethod(new PasswordTransformationMethod());
        mBinding.containerLoginLayout.btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == mBinding.containerLoginLayout.btnLogin.getId())
        {

            CommonFuntions.ActivitySwitching(mContext,MainActivity.class);
            Objects.requireNonNull(getActivity()).finish();
        }
    }
}
