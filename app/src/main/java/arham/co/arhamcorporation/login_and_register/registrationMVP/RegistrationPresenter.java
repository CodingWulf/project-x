package arham.co.arhamcorporation.login_and_register.registrationMVP;

public interface RegistrationPresenter {
    void getMobileOTPSentConfirmation(String mobileNumber);
    void optConfirmationView();
}
