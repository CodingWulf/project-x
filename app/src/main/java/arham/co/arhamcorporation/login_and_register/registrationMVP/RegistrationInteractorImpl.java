package arham.co.arhamcorporation.login_and_register.registrationMVP;

import android.app.Activity;
import android.content.Context;

import arham.co.arhamcorporation.common.FirebaseApis;
import arham.co.arhamcorporation.common.IActivityCommunication;

public class RegistrationInteractorImpl implements RegistrationInteractor, IActivityCommunication {

    private Context mContext;
    private Activity mActivity;
    private RegistrationInteractor.onMobileOTPSentResponse mListner;

    public RegistrationInteractorImpl(Context mContext,Activity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    @Override
    public void getMobileAuth(String mobileNumber) {

        FirebaseApis firebaseApis = FirebaseApis.getsInstance(mContext,mActivity);
        firebaseApis.doMobileAuth(mobileNumber,this);

    }

    @Override
    public void startOtpView(onMobileOTPSentResponse mListner) {
        this.mListner = mListner;
    }

    @Override
    public void callApiResutl(boolean result) {

        if (result)
        {
            mListner.onSuccess();
        }
        else {
            mListner.onFailure();
        }
    }
}

