package arham.co.arhamcorporation.login_and_register.registrationMVP;

public interface RegistrationInteractor  {

    interface onMobileOTPSentResponse{
        void onSuccess();
        void onFailure();
    }

    void getMobileAuth(String mobileNumber);
    void startOtpView(onMobileOTPSentResponse mListner);
}
