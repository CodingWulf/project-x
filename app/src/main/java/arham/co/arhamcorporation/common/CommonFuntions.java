package arham.co.arhamcorporation.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.view.Display;

public class CommonFuntions {

    public static void ActivitySwitching(Context mContext, Class<?> cls){

        Intent switchActivity = new Intent(mContext,cls);
        switchActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        mContext.startActivity(switchActivity);


    }

    public static float getScreenWidth(Activity mActivity)
    {
        float displayWidth;
        Display display = mActivity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        displayWidth = size.x / 2;
        return displayWidth;
       // mBinding.viewToAnimate.getLayoutParams().width = (int) displayWidth;
    }
}
