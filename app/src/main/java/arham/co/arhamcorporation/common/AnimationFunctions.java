package arham.co.arhamcorporation.common;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import arham.co.arhamcorporation.R;

public class AnimationFunctions {

    public static void fadeInAmination(View view, Context mContext)
    {
        Animation fadeIn = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        view.startAnimation(fadeIn);

    }

    public static void moveRight(View view,float displayWidth)
    {
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", displayWidth);
        animation.setDuration(400);
        animation.start();
    }
    public static void moveLeft(View view,float displayWidth)
    {
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", 0);
        animation.setDuration(400);
        animation.start();
    }



}
