package arham.co.arhamcorporation.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class FirebaseApis {

    @SuppressLint("StaticFieldLeak")
    private static FirebaseApis sInstance;
    @SuppressLint("StaticFieldLeak")
    private static Context mContext;
    private static final String TAG = "FireBaseApis";
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    boolean mobileStatus = false;
    private static Activity mActivity;

    public static synchronized FirebaseApis getsInstance(Context _Context,Activity _Activity)
    {
        mContext = _Context;
        sInstance = new FirebaseApis();
        mActivity = _Activity;
        return sInstance;
    }

    public void doMobileAuth(String mobileNumber,final IActivityCommunication currentActivity)
    {



        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

                Log.i(TAG,"Verification Completed");
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.i(TAG,"Verification Failed"+ e.getMessage());
            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                Log.i(TAG,"Verification Code Sent->> "+s+" ->> "+forceResendingToken);
            }
        };

        PhoneAuthProvider.getInstance().verifyPhoneNumber(mobileNumber
                ,60,TimeUnit.SECONDS,
                mActivity,mCallbacks);

        currentActivity.callApiResutl(mobileStatus);

    }
}
