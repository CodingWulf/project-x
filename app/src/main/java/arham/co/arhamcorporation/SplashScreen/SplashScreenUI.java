package arham.co.arhamcorporation.SplashScreen;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import arham.co.arhamcorporation.R;
import arham.co.arhamcorporation.common.AnimationFunctions;
import arham.co.arhamcorporation.common.CommonFuntions;
import arham.co.arhamcorporation.databinding.ActivitySplashScreenUiBinding;
import arham.co.arhamcorporation.login_and_register.LoginActivityUI;

public class SplashScreenUI extends AppCompatActivity {

    private ActivitySplashScreenUiBinding mBinding;
    private Context mContext;
    private SplashScreenUI mActivity;
    private static final int SPLASH_DURATION = 2000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mContext = this;
        mActivity = this;
        mBinding = DataBindingUtil.setContentView(mActivity, R.layout.activity_splash_screen_ui);

        //Animate the Image
        AnimationFunctions.fadeInAmination(mBinding.imageRupeesSymbol, mContext);
        AnimationFunctions.fadeInAmination(mBinding.appName, mContext);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                CommonFuntions.ActivitySwitching(mContext, LoginActivityUI.class);
                finish();
            }
        }, SPLASH_DURATION);


    }


}
