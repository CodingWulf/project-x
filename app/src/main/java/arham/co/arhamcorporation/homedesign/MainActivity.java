package arham.co.arhamcorporation.homedesign;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import arham.co.arhamcorporation.R;
import arham.co.arhamcorporation.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity  {

    private ActivityMainBinding mainBinding;
    private Context mContext;
    private MainActivity mActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mActivity = this;
        mainBinding = DataBindingUtil.setContentView(mActivity, R.layout.activity_main);
        renderView();
    }

    private void renderView() {
        StartAnimations();
        mainBinding.bottomAppBar.replaceMenu(R.menu.menu);


        mainBinding.bottomAppBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

               switch (menuItem.getItemId())
               {
                   case R.id.navSearch:
                       Toast.makeText(mContext,"Search Icon",Toast.LENGTH_SHORT).show();
               }

                return true;
            }
        });

        mainBinding.bottomAppBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* PopupMenu popup = new PopupMenu(MainActivity.this, mainBinding.bottomAppBar);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(MainActivity.this,"You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });

                popup.show();*/
               /* sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                sheetBehavior.setHideable(false);*/

               BottomSheetFrament bottomSheetFrament = new BottomSheetFrament();
               bottomSheetFrament.show(getSupportFragmentManager(),bottomSheetFrament.getTag());

            }
        });

    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        anim.reset();
        mainBinding.mainActivity.clearAnimation();
        mainBinding.mainActivity.startAnimation(anim);
        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();

    }
}
