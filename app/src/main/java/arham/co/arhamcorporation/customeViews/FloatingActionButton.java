package arham.co.arhamcorporation.customeViews;

import android.content.Context;
import android.util.AttributeSet;

public class FloatingActionButton extends com.google.android.material.floatingactionbutton.FloatingActionButton {
    public FloatingActionButton(Context context) {
        super(context);
    }

    public FloatingActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    private void init()
    {

    }
}
