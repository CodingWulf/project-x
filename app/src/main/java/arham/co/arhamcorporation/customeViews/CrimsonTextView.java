package arham.co.arhamcorporation.customeViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CrimsonTextView extends TextView {

    public CrimsonTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CrimsonTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CrimsonTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "CrimsonText-Regular.ttf");
        setTypeface(tf);
    }
}
