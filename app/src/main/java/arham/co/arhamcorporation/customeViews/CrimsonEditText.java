package arham.co.arhamcorporation.customeViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputEditText;

public class CrimsonEditText extends TextInputEditText {

    public CrimsonEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CrimsonEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CrimsonEditText(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "CrimsonText-Regular.ttf");
        setTypeface(tf);
    }
}
